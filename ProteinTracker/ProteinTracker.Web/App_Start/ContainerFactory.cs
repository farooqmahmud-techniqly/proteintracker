﻿using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Jil;

namespace ProteinTracker.Web
{
    public sealed class ContainerFactory
    {
        public static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterSource(new ViewRegistrationSource());
            builder.RegisterFilterProvider();

            RegisterRedisDependencies(builder);

            var container = builder.Build();

            return container;
        }

        private static void RegisterRedisDependencies(ContainerBuilder builder)
        {
            var mux = ConnectionMultiplexer.Connect(new ConfigurationOptions
            {
                EndPoints = {{"127.0.0.1", 6379}},
                DefaultDatabase = 0
            });

            var ser = new JilSerializer();
            var client = new StackExchangeRedisCacheClient(mux, ser);
            builder.RegisterInstance(client).As<ICacheClient>().SingleInstance();
        }
    }
}