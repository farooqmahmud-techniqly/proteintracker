﻿using System.Web.Mvc;
using ProteinTracker.Web.Models;
using StackExchange.Redis.Extensions.Core;

namespace ProteinTracker.Web.Controllers
{
    public class UsersController : Controller
    {
        private readonly ICacheClient _cacheClient;
        
        public UsersController(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient;
        }

        public ActionResult NewUser()
        {
            return View();
        }

        public ActionResult Add(string username, int goal, long userId = 0)
        {
            User user = null;
            var userKey = $"user:{userId}";

            if (userId == 0)
            {
                user = _cacheClient.Get<User>(userKey);
            }
            else
            {
                user = new User
                {
                    Id = GetNextId()
                };

                userKey = $"user:{user.Id}";
            }

            user.Name = username;
            user.Goal = goal;

            _cacheClient.Add(userKey, user);

            var userSetKey = $"Users";
            _cacheClient.Database.SetAdd(userSetKey, user.Id);
            
            return RedirectToAction("Index", "Tracker", new {user.Id});
        }

        private long GetNextId()
        {
            var userIdsKey = "seq:Users";
            return _cacheClient.Database.StringIncrement(userIdsKey);
        }

        public ActionResult Edit(int userId)
        {
            var userKey = $"user:{userId}";
            var user = _cacheClient.Get<User>(userKey);
            ViewBag.Username = user.Name;
            ViewBag.Goal = user.Goal;
            ViewBag.UserId = user.Id;

            return View("NewUser");
        }
    }
}