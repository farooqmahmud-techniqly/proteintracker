﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProteinTracker.Web.Models;
using StackExchange.Redis.Extensions.Core;

namespace ProteinTracker.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICacheClient _cacheClient;
        
        public HomeController(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult SelectUser()
        {

            var userSetKey = $"Users";
            var usersIds = _cacheClient.SetMembers<long>(userSetKey);
            var users = _cacheClient.GetAll<User>(usersIds.Select(id => $"user:{id}"));
            var usersSelection = new SelectList(users.Values, "Id", "Name", string.Empty);
            ViewBag.UserId = usersSelection;

            return View();
        }
    }
}