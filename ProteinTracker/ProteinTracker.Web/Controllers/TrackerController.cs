﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProteinTracker.Web.Models;
using StackExchange.Redis.Extensions.Core;

namespace ProteinTracker.Web.Controllers
{
    public class TrackerController : Controller
    {
        private readonly ICacheClient _cacheClient;

        public TrackerController(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient;
        }

        // GET: Tracker
        public ActionResult Index(long userId, int amount = 0)
        {
            var userKey = $"user:{userId}";
            var user = _cacheClient.Get<User>(userKey);

            if(amount > 0)
            {
                user.Total += amount;
                _cacheClient.Add(userKey, user);
            }

            ViewBag.Username = user.Name;
            ViewBag.Total = user.Total;
            ViewBag.Goal = user.Goal;
            ViewBag.UserId = user.Id;
            return View();
        }
    }
}